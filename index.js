/*
	You can declare functions by using:
	1. function keyword
	2. function name
	3. open/close parehthesis
	4. open/close curly braces 
*/

function sayHello(){
	console.log('Hello there!');
};

// You can invoke a function by callings its function name and including the parethesis
sayHello();

// You can assign a function to a variable. The function name would not be required.
let sayGoodbye = function () {
	console.log('Goodbye!');
};

// You cna invoke a function inside a variable by calling its variable name + ()
sayGoodbye();

// You can also re-assign a function a new value of a variable
sayGoodbye = function () {
	console.log('Au Revior!');
};

sayGoodbye();

// declairing a constant variable with a fucntion as a value will not allow that function to be changed or re-assigned
// const sayHelloInJapanese = function () {
// 	console.log('Ohayo!');
// };

// // // sayHelloInJapanese = function () {
// // 	console.log('Kumusta!');
// // // };
// sayHelloInJapanesey();


// Global scope - You can use a variable inside a function if the variable is declared outside of it
let action = 'Run';

function doSomethingRandom () {
	console.log(action);
};


doSomethingRandom()


// Local Scope - you cannot use a variable outside of a function if it is within the function scope
function doSomethingRandom () {
	let action = 'Run';
	console.log(action);
};

doSomethingRandom();


// You can nest a function inside of a new function as long as you invoke the child function within the scope of the parent function
// Not a good practice to have too many nested functions

function viewProduct () {
	console.log('Viewing a Product');
	function addToCart () {
		console.log('Added a product to cart')
	};
	addToCart();
}
viewProduct();


// Alert Function is a built-in Javascript function where we can show alerts to the user
function singASong () {
	alert('La la la');
};

singASong();
//Any statement like 'console.log' will run only after the alert has been closed; if it's invoked below the alert function
console.log('Clap clap clap');

//Promt is a built-in javascript function that we can use to take input from the user

function enterUserName(){
	let userName = prompt('Enter your username')
	console.log(userName)
}
enterUserName()

// You can use a prompt outside a function. Make sure to output the prompt value by assigning to a variable and using console.log
let userAge = prompt('Enter you age')
console.log(userAge)

// shorthand - console.log(prompt('Enter your age'))